import numpy as np
from tqdm import tqdm


def _chunk_indices(freq, chunk_size=200000):
    freq_ravel = freq.ravel()

    freq_indices_sorted = np.argsort(-freq_ravel)

    total = 0
    chunks = []
    batch_chunk_indices = []
    for freq_id in freq_indices_sorted:
        freq = freq_ravel[freq_id]

        total += freq
        batch_chunk_indices.append(freq_id)

        if total > chunk_size:
            chunks.append(batch_chunk_indices)

            total = 0
            batch_chunk_indices = []

    if len(batch_chunk_indices) > 0:
        chunks.append(batch_chunk_indices)

    return chunks


def _get_grid_intervals(x, y):
    def make_intervals(array):
        left = array[:-1]
        right = array[1:]

        return list(zip(left, right))

    x_ints, y_ints = make_intervals(x), make_intervals(y)

    all_intvals = []

    for r in tqdm(x_ints):
        for c in y_ints:
            all_intvals.append((r, c))

    all_intvals = np.array(all_intvals)

    return all_intvals


def _select_chunk_indices(x, y, chunk_intervals):
    indices = []

    for chunk_intval in chunk_intervals:
        x_l, x_r = chunk_intval[0]
        y_l, y_r = chunk_intval[1]

        mask = (x >= x_l) & (x < x_r) & (y >= y_l) & (y < y_r)
        ids = np.argwhere(mask == 1)
        indices.extend(ids)

    indices = np.array(indices)

    return indices.squeeze()


def _split_by_density_rings(x, y, bins=200, chunk_size=200000):
    freq, x_range, y_range = np.histogram2d(x, y, bins=bins)
    chunks = _chunk_indices(freq, chunk_size=chunk_size)

    intvals = _get_grid_intervals(x_range, y_range)

    rings_indices = []
    for chunk in tqdm(chunks):
        intval = intvals[chunk]
        ids = _select_chunk_indices(x, y, intval)
        rings_indices.append(ids)

    return rings_indices


def split_by_density_rings(
        data,
        chunk_size=200000,
        x_key='Brunel_P',
        y_key='Brunel_ETA'):

    x = data[x_key].to_numpy()
    y = data[y_key].to_numpy()

    rings_indices = _split_by_density_rings(x, y, chunk_size=chunk_size)

    data_rings = []
    for ring_ids in rings_indices:
        data_rings.append(data.iloc[ring_ids])

    print(f"Len data rings: {len(data_rings)}")

    return data_rings, rings_indices

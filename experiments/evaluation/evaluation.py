import numpy as np
import pandas as pd
from src.datasets import utils_rich
from src.evaluation import eval_model_no_bins


def single_model_evaluation(feat, target, predict, scaler, cols=None, verbose=True):
    pred_data = np.concatenate([predict, feat], axis=1)
    val_data = np.concatenate([target, feat], axis=1)

    pred_data_inversed = scaler.inverse_transform(pred_data)
    val_data_inversed = scaler.inverse_transform(val_data)

    if cols is None:
        cols = ['RichDLLe', 'RichDLLk', 'RichDLLmu', 'RichDLLp', 'RichDLLbt',
                'Brunel_P', 'Brunel_ETA', 'nTracks_Brunel']

    val_data_inversed_ = pd.DataFrame(val_data_inversed,
                                      columns=cols)
    for ind, col in enumerate(utils_rich.dll_columns):
        val_data_inversed_["predicted_"+col] = pred_data_inversed[:, ind]
    val_data_inversed_['probe_sWeight'] = 1

    scores = eval_model_no_bins(val_data_inversed_)

    if verbose:
        sc_mean, sc_std = scores.mean(axis=0)[0][1], scores.std(axis=0)[0][1]
        print(f"Ensemble test score for model is: {sc_mean} +- {sc_std}")

    return scores


def ensemble_evaluation(feat, preds, preds2, scaler, cols_in=None, cols_out=None, verbose=True):
    preds_data = []
    for pred_data in preds2:
        print(f"Pred data shape: {pred_data.shape}, feat shape: {feat.shape}")
        preds_data.append(np.concatenate([pred_data, feat], axis=1))

    preds_data_inversed = [
        scaler.inverse_transform(data) for data in preds_data]

    preds_gt = np.concatenate(preds, axis=0)
    ensemble_size = len(preds)
    val_data = np.concatenate(
        [preds_gt, np.tile(feat, (ensemble_size, 1))], axis=1)
    val_data_inversed = scaler.inverse_transform(val_data)

    if cols_out is None:
        cols_out = ['RichDLLe', 'RichDLLk',
                    'RichDLLmu', 'RichDLLp', 'RichDLLbt']

    if cols_in is None:
        cols_in = ['Brunel_P', 'Brunel_ETA', 'nTracks_Brunel']

    def create_df(data):
        cols_out_pred = ["predicted_" + col for col in cols_out]

        df = pd.DataFrame(data,
                          columns=cols_out_pred + cols_in)
        df["probe_sWeight"] = 1

        return df

    preds_data_inversed_ = [create_df(pdata) for pdata in preds_data_inversed]

    val_data_inversed_ = pd.DataFrame(val_data_inversed,
                                      columns=cols_out + cols_in)
    val_data_inversed_["probe_sWeight"] = 1

    scores_total = []
    for i in range(len(preds)):
        n_sample = preds_data_inversed_[i].shape[0]

        val_data_sample = val_data_inversed_.sample(n=n_sample)

        scores = eval_model_no_bins(val_data_sample, preds_data_inversed_[i])
        scores_total.append(scores)

        if verbose:
            sc_mean, sc_std = scores.mean(
                axis=0)[0][1], scores.std(axis=0)[0][1]
            print(
                f"Ensemble test score for model {i} is: {sc_mean} +- {sc_std}")

    return scores_total

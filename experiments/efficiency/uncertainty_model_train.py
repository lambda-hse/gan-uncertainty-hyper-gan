import tensorflow as tf
import tensorflow_addons as tfa

from tensorflow.keras.metrics import mean_absolute_error, mean_squared_error
from tensorflow.keras.optimizers import Adam


def train_model(model, x_train, y_train, save_path, lr=1e-4, 
                batch_size=int(1e3), epochs=100, verbose=0, show_epoch_progress=False):
    optimizer = Adam(lr)

    loss = mean_squared_error
    metrics = [mean_squared_error, mean_absolute_error]

    model.compile(optimizer=optimizer, loss=loss, metrics=metrics)

    rop = tf.keras.callbacks.ReduceLROnPlateau(
        monitor="val_loss",
        factor=0.5,
        patience=5,
        verbose=verbose,
        min_delta=0.001,
        min_lr=1e-6
    )

    ckpt = tf.keras.callbacks.ModelCheckpoint(
        save_path, monitor='val_loss', verbose=verbose, save_best_only=True,
        save_weights_only=True, mode='auto', save_freq='epoch'
    ) 

    pbar = tfa.callbacks.TQDMProgressBar(
        show_epoch_progress=show_epoch_progress, 
        leave_overall_progress=False)

    callbacks = [rop, ckpt, pbar]

    config = {
        'x': x_train,
        'y': y_train,
        'batch_size': batch_size,
        'validation_split': 0.25,
        'epochs': epochs,
        'verbose': 0,
        'callbacks': callbacks,
        'shuffle': True,
    }

    history = model.fit(**config)

    return history

from functools import partial

import numpy as np
import tensorflow as tf
from tensorflow.keras import backend as K
from tensorflow.keras.layers import Dense, Input, LeakyReLU, ReLU


def bernoulli_dropout(inputs, rate):
    return K.dropout(inputs, rate)


def bernoulli_dropout_structured(inputs, rate, patch_size=3):
    mask = tf.random.uniform(tf.shape(inputs)) < rate
    mask = tf.cast(mask, tf.float32)

    mask = tf.nn.conv1d(mask[:, :, None],
                        tf.ones([patch_size, 1, 1]),
                        stride=1, padding='SAME')[:, :, 0]
    mask = 1 - tf.clip_by_value(mask, 0, 1)

    # TODO: replace with analyticaly computed probability
    # TODO: based on rate and patch size
    keep_prob = tf.reduce_mean(mask)

    mask = mask / keep_prob

    return mask


def gaussian_dropout(inputs, rate):
    stddev = np.sqrt(rate / (1.0 - rate))

    return inputs * K.random_normal(
        shape=tf.shape(inputs),
        mean=1.0,
        stddev=stddev,
        dtype=inputs.dtype)


class DropoutTrain(tf.keras.layers.Layer):
    _dropouts = {
        'bernoulli': bernoulli_dropout,
        'gaussian': gaussian_dropout,
        'bernoulli_structured': bernoulli_dropout_structured
    }

    def __init__(self, rate, dropout_type='gaussian', name=None, **kwargs):
        super().__init__(name=name)

        if dropout_type not in self._dropouts.keys():
            dropout_types = list(self._dropouts.keys())
            raise ValueError(
                f'dropout_type parameter should be in {dropout_types}')

        self.dropout_fn = partial(
            self._dropouts[dropout_type], rate=rate, **kwargs)

        self._reset_dropout_mask()

        self._update_mask = True
        self._elementwise = True

    def _reset_dropout_mask(self):
        self.drop_mask = None

    def call(self, inputs):
        if self._update_mask or self.drop_mask is None:
            # print("In update mask")
            input_shape = tf.shape(inputs)

            if self._elementwise:
                mask = tf.ones(input_shape)
                # print("Created mask in elementwise mode")
            else:
                mask = tf.expand_dims(tf.ones(input_shape[1:]), axis=0)
                # print("Created mask in nonelementwise mode")

            self.drop_mask = self.dropout_fn(mask)

        # print("Applying mask")
        outs = self.drop_mask[:inputs.shape[0]] * inputs

        return outs


class NoiseInjection(tf.keras.layers.Layer):
    def __init__(self, noise_dim, name='NoiseInjection'):
        super().__init__(name=name)

        self.noise_dim = noise_dim

    def call(self, x):
        noise = tf.random.normal(
            [tf.shape(x)[0], self.noise_dim], name='noise')

        return tf.concat([x, noise], axis=1, name='x_plus_noise')


class VirtualEnsembleModel(tf.keras.Model):
    def _set_update_mask_and_elementwise(
        self,
        upd_mask: bool = True,
        elwise: bool = True
    ):
        for layer in self.layers:
            if hasattr(layer, '_update_mask'):
                # print("Found attr update_mask")
                setattr(layer, '_update_mask', upd_mask)

            if hasattr(layer, '_elementwise'):
                setattr(layer, '_elementwise', elwise)

    def _reset_dropout(self):
        for layer in self.layers:
            if hasattr(layer, 'drop_mask'):
                setattr(layer, 'drop_mask', None)

    def train_mode(self):
        self._set_update_mask_and_elementwise(True, True)

    def predict_mode(self):
        self._reset_dropout()
        self._set_update_mask_and_elementwise(False, True)

    def single_model_inference_mode(self):
        self._reset_dropout()
        self._set_update_mask_and_elementwise(False, False)

    def ensemble_inference_mode(self):
        self._set_update_mask_and_elementwise(True, False)


def dense_act_dropout(x, name, units, rate, drop_type, drop_kwargs):
    x = Dense(units, name=f'{name}/Dense')(x)
    x = LeakyReLU(0.05, name=f'{name}/LeakyReLU')(x)
    x = DropoutTrain(
        rate, drop_type, name=f'{name}/DropoutTrain', **drop_kwargs)(x)

    return x


def RichMCDropFunc(input_shape=(3,), noise_dim=64, output_dim=5, units=128, num_hidden=5,
                   drop_rate=0.01, dropout_type='bernoulli', drop_kwargs={}):

    inputs = tf.keras.Input(input_shape, name='Inputs')

    x = NoiseInjection(noise_dim)(inputs)
    for i in range(num_hidden):
        print(f'Layer {i}')
        x = dense_act_dropout(x, f'Layer_{i}', units,
                              drop_rate, dropout_type, drop_kwargs)

    outputs = Dense(output_dim, name='DensePrediction')(x)

    return VirtualEnsembleModel(inputs=inputs, outputs=outputs)


class DenseVarDropout(tf.keras.layers.Layer):
    k1, k2, k3 = 0.63576, 1.8732, 1.48695
    C = -k1

    def __init__(
        self,
        units,
        ard_init=-10.,
        activation=ReLU(),
        name='DenseVarDropout',
        **kwargs
    ):
        super().__init__(name=name, **kwargs)

        self.units = units
        self.ard_init = ard_init

        self.activation = activation
        self.elementwise = True

    def build(self, input_shape):
        num_feats = input_shape[1]

        self.kernel = self.add_weight(
            name='kernel',
            shape=[num_feats, self.units],
            dtype=tf.float32,
            initializer=tf.keras.initializers.GlorotNormal()
        )

        self.bias = self.add_weight(name='bias',
                                    shape=[self.units],
                                    dtype=tf.float32,
                                    initializer='zeros')

        self.log_sigma2 = self.add_weight(
            name='ls2',
            shape=[num_feats, self.units],
            dtype=tf.float32,
            initializer=tf.keras.initializers.Constant(self.ard_init)
        )

        super().build(input_shape)

    def call(self, x, deterministic=False, threshold=3.):
        log_alpha = self.log_sigma2 - tf.math.log(tf.square(self.kernel))
        log_alpha = tf.clip_by_value(log_alpha, -8., 8.)

        if deterministic:
            clip_mask = log_alpha >= threshold
            x = tf.matmul(x, tf.where(
                clip_mask, tf.zeros_like(self.kernel), self.kernel))
        else:
            mu = tf.matmul(x, self.kernel)
            si = tf.matmul(x * x, tf.math.exp(log_alpha)
                           * self.kernel * self.kernel)
            si = tf.math.sqrt(si + 1e-8)

            noise_shape = tf.shape(mu)

            if self.elementwise is False:
                noise_shape = tf.concat([[1], noise_shape[1:]], axis=0)

            x = mu + tf.random.normal(noise_shape) * si

        return self.activation(x + self.bias)

    def reg_loss(self):
        log_alpha = self.log_sigma2 - tf.math.log(tf.square(self.kernel))
        log_alpha = tf.clip_by_value(log_alpha, -8., 8.)
        mdkl = self.k1 * tf.nn.sigmoid(self.k2 + self.k3 * log_alpha) - \
            0.5 * tf.math.log1p(tf.math.exp(-log_alpha)) + self.C

        return -tf.reduce_sum(mdkl)


def RichVarMCDropFunc(input_shape=(3,), noise_dim=64, output_dim=5, num_layers=5):
    inputs = Input(shape=input_shape)

    noise = tf.random.normal([tf.shape(inputs)[0], noise_dim])
    features = tf.concat([inputs, noise], axis=1)

    layers = tf.keras.Sequential()

    for i in range(5):
        layers.add(DenseVarDropout(128, name='DenseVarDropout' + f'{i + 1}'))

    layers.add(Dense(output_dim))

    outputs = layers(features)
    model = tf.keras.Model(inputs=inputs, outputs=outputs)

    for layer in model.layers[-1].layers:
        if hasattr(layer, 'reg_loss'):
            print("Found attr")
            model.add_loss(layer.reg_loss)

    return model


def _set_elementwise(model, value=True):
    for layer in model.layers[-1].layers:
        if hasattr(layer, 'elementwise'):
            print("Found attr elementwise")
            layer.elementwise = value


def ensemble_inference_mode(model):
    _set_elementwise(model, value=False)


def ensemble_training_mode(model):
    _set_elementwise(model, value=True)
